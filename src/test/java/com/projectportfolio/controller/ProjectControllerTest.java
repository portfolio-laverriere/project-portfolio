package com.projectportfolio.controller;

import com.projectportfolio.exception.ProjectConflictException;
import com.projectportfolio.exception.ProjectNotFoundException;
import com.projectportfolio.model.ProjectEntity;
import com.projectportfolio.model.ResourceEntity;
import com.projectportfolio.proxy.MicroserviceMediaProxy;
import com.projectportfolio.repository.ProjectRepository;
import com.projectportfolio.repository.ResourceRepository;
import com.projectportfolio.service.dto.ImageDTO;
import com.projectportfolio.service.dto.ProjectDTO;
import com.projectportfolio.service.dto.ResourceDTO;
import com.projectportfolio.service.mapper.ProjectMapper;
import com.projectportfolio.service.mapper.ResourceMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

class ProjectControllerTest {

    @Mock
    ProjectRepository projectRepository;

    @Mock
    ResourceRepository resourceRepository;

    @Mock
    MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    ProjectController projectController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllProject() {

        List<ProjectDTO> projectMockList = new ArrayList<>();
        List<ResourceDTO> resourceMockList = new ArrayList<>();
        List<ResourceDTO> resourceMockList2 = new ArrayList<>();
        List<ImageDTO> imageMockList = new ArrayList<>();
        List<ImageDTO> imageMockList2 = new ArrayList<>();

        /* --------------------------------------- MOCK 1 PROJECT ---------------------------------------- */
        ProjectDTO projectMock1 = ProjectDTO.builder().id(1).name("Projet test mock 1")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer pour test").build();

        /* --------------------------------------- MOCK 1 RESOURCE ---------------------------------------- */
        ResourceDTO resourceMock1 = ResourceDTO.builder().id(1).name("Git").url("http://git.com").build();
        resourceMockList.add(resourceMock1);
        projectMock1.setResourceDTOList(resourceMockList);

        /* --------------------------------------- MOCK 1 IMAGE ---------------------------------------- */
        ImageDTO imageMock1 = ImageDTO.builder().id(1).name("projet1").imagePath("path_image").build();
        imageMockList.add(imageMock1);
        projectMock1.setImageDTOList(imageMockList);

        projectMockList.add(projectMock1);

        /* --------------------------------------- MOCK 2 PROJECT ---------------------------------------- */
        ProjectDTO projectMock2 = ProjectDTO.builder().id(2).name("Projet test mock 2")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer pour test").build();

        /* --------------------------------------- MOCK 2 RESOURCE ---------------------------------------- */
        ResourceDTO resourceMock2 = ResourceDTO.builder().id(2).name("Git").url("http://git.com").build();
        resourceMockList2.add(resourceMock2);
        projectMock2.setResourceDTOList(resourceMockList2);

        /* --------------------------------------- MOCK 2 IMAGE ---------------------------------------- */
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("projet2").imagePath("path_image").build();
        imageMockList2.add(imageMock2);
        projectMock2.setImageDTOList(imageMockList2);

        projectMockList.add(projectMock2);

        when(projectRepository.findAll())
                .thenReturn(ProjectMapper.INSTANCE.toEntityList(projectMockList));

        when(microserviceMediaProxy.getListImageByProject(projectMock1.getId()))
                .thenReturn(imageMockList);
        when(resourceRepository.findAllResourceByProjectId(projectMock1.getId()))
                .thenReturn(ResourceMapper.INSTANCE.toEntityList(resourceMockList));

        when(microserviceMediaProxy.getListImageByProject(projectMock2.getId()))
                .thenReturn(imageMockList2);
        when(resourceRepository.findAllResourceByProjectId(projectMock2.getId()))
                .thenReturn(ResourceMapper.INSTANCE.toEntityList(resourceMockList2));

        final List<ProjectDTO> projectDTOList = projectController.getAllProject();

        Assertions.assertEquals(projectDTOList.size(), 2);
        Assertions.assertEquals(projectDTOList.get(0).getResourceDTOList().get(0).getId(), 1);
        Assertions.assertEquals(projectDTOList.get(0).getImageDTOList().get(0).getId(), 1);
        Assertions.assertEquals(projectDTOList.get(1).getResourceDTOList().get(0).getId(), 2);
        Assertions.assertEquals(projectDTOList.get(1).getImageDTOList().get(0).getId(), 2);
    }

    @Test
    void addProject() {

        ProjectEntity projectEntityMock = ProjectEntity.builder().id(8).name("Nom du projet")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer du projet").build();

        ProjectDTO projectDTOMock = ProjectDTO.builder().name("Nom du projet")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer du projet").build();

        when(projectRepository.findByName(projectDTOMock.getName())).thenReturn(null);
        when(projectRepository.save(any())).thenReturn(projectEntityMock);

        final ProjectDTO projectDTO = projectController.addProject(projectDTOMock);
        Assertions.assertEquals(projectDTO.getId(), projectEntityMock.getId());

        when(projectRepository.findByName(projectDTOMock.getName())).thenReturn(projectEntityMock);
        Assertions.assertThrows(ProjectConflictException.class, () -> {
            projectController.addProject(projectDTOMock);
        });
    }

    @Test
    void addResourceForProject() {

        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        Integer projectIdMock = 3;

        ResourceEntity resourceEntityMock = ResourceEntity.builder().id(2).name("Nom de la ressource")
                .url("URL de la ressource").build();
        ResourceDTO resourceDTOMock = ResourceDTO.builder().name("Nom de la ressource").url("URL de la ressource").build();
        resourceDTOListMock.add(resourceDTOMock);


        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceProjectExists(resourceEntityMock.getId(), projectIdMock)).thenReturn(false);
        when(resourceRepository.joinResourceToTheProject(resourceEntityMock.getId(), projectIdMock)).thenReturn(1);

        final List<ResourceDTO> resourceDTOList1 = projectController.addResourceForProject(resourceDTOListMock, projectIdMock);
        for (ResourceDTO resourceDTO : resourceDTOList1) {
            Assertions.assertEquals(resourceDTO.getId(), 2);
        }

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(null);
        when(resourceRepository.projectExists(projectIdMock)).thenReturn(true);
        when(resourceRepository.save(any())).thenReturn(resourceEntityMock);
        when(resourceRepository.joinResourceToTheProject(resourceEntityMock.getId(), projectIdMock)).thenReturn(1);

        final List<ResourceDTO> resourceDTOList2 = projectController.addResourceForProject(resourceDTOListMock, projectIdMock);
        for (ResourceDTO resourceDTO : resourceDTOList2) {
            Assertions.assertEquals(resourceDTO.getId(), 2);
        }
    }

    @Test
    void addResourceForProjectException() {

        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        Integer projectIdMock  = 5;

        ResourceEntity resourceEntityMock = ResourceEntity.builder().id(1).name("Nom de la ressource")
                .url("URL de la ressource").build();
        ResourceDTO resourceDTOMock = ResourceDTO.builder().name("Nom de la ressource")
                .url("URL de la ressource").build();
        resourceDTOListMock.add(resourceDTOMock);

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceProjectExists(resourceEntityMock.getId(), projectIdMock)).thenReturn(true);
        Assertions.assertThrows(ProjectConflictException.class, () -> {
            projectController.addResourceForProject(resourceDTOListMock, projectIdMock);
        });

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceProjectExists(resourceEntityMock.getId(), projectIdMock)).thenReturn(false);
        when(resourceRepository.joinResourceToTheProject(resourceEntityMock.getId(), projectIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ProjectNotFoundException.class, () -> {
            projectController.addResourceForProject(resourceDTOListMock, projectIdMock);
        });

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(null);
        when(resourceRepository.resourceProjectExists(resourceEntityMock.getId(), projectIdMock)).thenReturn(true);
        when(resourceRepository.projectExists(projectIdMock)).thenReturn(false);
        Assertions.assertThrows(ProjectNotFoundException.class, () -> {
            projectController.addResourceForProject(resourceDTOListMock, projectIdMock);
        });
    }

    @Test
    void upProject() {

        ProjectDTO projectMock = ProjectDTO.builder().id(1).name("Projet test mock 1")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer pour test").build();

        when(projectRepository.existsById(projectMock.getId())).thenReturn(true);
        when(projectRepository.save(any())).thenReturn(ProjectMapper.INSTANCE.toEntity(projectMock));
        final ProjectDTO projectDTO = projectController.upProject(projectMock);
        Assertions.assertEquals(projectDTO.getId(), projectMock.getId());

        when(projectRepository.existsById(projectMock.getId())).thenReturn(false);
        Assertions.assertThrows(ProjectNotFoundException.class, () -> {
            projectController.upProject(projectMock);
        });
    }

    @Test
    void delProject() {

        Integer projectIdMock = 1;

        when(projectRepository.deleteProjectById(projectIdMock)).thenReturn(1);
        final String checkStatusSuccess = projectController.delProject(projectIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(projectRepository.deleteProjectById(projectIdMock)).thenReturn(0);
        final String checkStatusFailure = projectController.delProject(projectIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }

    @Test
    void delResourceByProject() {

        Integer resourceIdMock = 1;
        Integer projectIdMock = 8;

        when(resourceRepository.deleteResourceByProject(resourceIdMock, projectIdMock)).thenReturn(1);
        final String checkStatusSuccess = projectController.delResourceByProject(resourceIdMock, projectIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(resourceRepository.deleteResourceByProject(resourceIdMock, projectIdMock)).thenReturn(0);
        final String checkStatusFailure = projectController.delResourceByProject(resourceIdMock, projectIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
