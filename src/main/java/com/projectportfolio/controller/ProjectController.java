package com.projectportfolio.controller;

import com.projectportfolio.exception.ProjectConflictException;
import com.projectportfolio.exception.ProjectNotFoundException;
import com.projectportfolio.model.ProjectEntity;
import com.projectportfolio.model.ResourceEntity;
import com.projectportfolio.proxy.MicroserviceMediaProxy;
import com.projectportfolio.repository.ProjectRepository;
import com.projectportfolio.repository.ResourceRepository;
import com.projectportfolio.service.dto.ProjectDTO;
import com.projectportfolio.service.dto.ResourceDTO;
import com.projectportfolio.service.mapper.ProjectMapper;
import com.projectportfolio.service.mapper.ResourceMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES PROJECTS")
@RestController
public class ProjectController {

    private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET ALL PROJECT ---------------------------------------- */
    @ApiOperation( value = "GET ALL PROJECT" )
    @GetMapping(value = "/project/project/get-all-project")
    public List<ProjectDTO> getAllProject() {

        List<ProjectDTO>  projectDTOList = new ArrayList<>();

        try {
            /**
             * JE RECUPERE TOUT LES PROJETS
             * @see ProjectRepository
             */
            projectDTOList = ProjectMapper.INSTANCE.toDTOList(projectRepository.findAll());

            for (ProjectDTO projectDTO : projectDTOList) {
                /**
                 * JE RECUPERE LA LISTE DES IMAGES POUR CHAQUE PROJET
                 * @see MicroserviceMediaProxy#getListImageByProject(Integer)
                 */
                projectDTO.setImageDTOList(microserviceMediaProxy.getListImageByProject(projectDTO.getId()));

                /**
                 * JE RECUPERE LA LISTE DES RESSOURCES POUR CHAQUE PROJET
                 * @see ResourceRepository#findAllResourceByProjectId(Integer)
                 */
                projectDTO.setResourceDTOList(ResourceMapper.INSTANCE.toDTOList(
                        resourceRepository.findAllResourceByProjectId(projectDTO.getId())));
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return projectDTOList;
    }


                                    /* ===================================== */
                                    /* ================ ADD ================ */
                                    /* ===================================== */

    /* ------------------------------------------ ADD PROJECT ------------------------------------------ */
    @ApiOperation( value = "ADD PROJECT")
    @PostMapping(value = "/project/project/add-project")
    public ProjectDTO addProject(@RequestBody ProjectDTO projectDTO) {

        /** JE RECUPERE LA DATE COURANTE LORS DE LA CREATION D'UN NOUVEAU PROJET */
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        projectDTO.setPublication(timestamp);

        ProjectEntity projectEntity = projectRepository.findByName(projectDTO.getName());

        if (projectEntity == null) {

            try {

                /** J'AJOUTE LE PROJET, PUIS JE RECUPERE SON IDENTIFIANT */
                projectDTO.setId(ProjectMapper.INSTANCE.toDTO
                        (projectRepository.save(ProjectMapper.INSTANCE.toEntity(projectDTO))).getId());

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProjectConflictException("The project '" + projectDTO.getName() + "' already exists !");
        }
        return projectDTO;
    }

    /* ------------------------------------- ADD RESSOURCE FOR PROJECT ------------------------------------- */
    @ApiOperation(value = "ADD RESOURCE FOR PROJECT")
    @PostMapping(value = "/project/add-resource/{projectId}")
    public List<ResourceDTO> addResourceForProject(@RequestBody List<ResourceDTO> resourceDTOList,
                                                  @PathVariable Integer projectId) {

        for (ResourceDTO resourceDTO : resourceDTOList) {

            /**
             * JE VERIFIE SI LA RESSOURCE EST DEJA PRESENTE DANS LA TABLE
             * @see ResourceRepository#findByUrl(String)
             */
            ResourceEntity resourceEntity = resourceRepository.findByUrl(resourceDTO.getUrl());

            /**
             * SI LA RESSOURCE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE PROJECT DANS
             * LA TABLE DE JOINTURE "RESOURCE_PROJECT", SINON J'AJOUTE LA NOUVELLE RESSOURCE ET JE CREE LA JOINTURE
             */
            if (resourceEntity != null) {

                /**
                 * JE VERIFIE SI RESOURCE ET PROJECT SON DEJA LIES
                 * @see ResourceRepository#resourceProjectExists(Integer, Integer)
                 */
                Boolean resourceProjectExists = resourceRepository.resourceProjectExists(resourceEntity.getId(), projectId);

                if (!resourceProjectExists) {

                    try {

                        /** @see ResourceRepository#joinResourceToTheProject(Integer, Integer)  */
                        resourceRepository.joinResourceToTheProject(resourceEntity.getId(), projectId);
                        resourceDTO.setId(resourceEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        logger.error(String.valueOf(pEX));
                        throw new ProjectNotFoundException("Project not found for the ID : " + projectId +
                                " - Impossible to link the resource to the project");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ProjectConflictException("The resource is already linked to the project : " + projectId);
                }

            } else {

                /**
                 * JE VERIFIE SI LE PROJET EXISTE
                 * @see ResourceRepository#articleExists(Integer)
                 */
                Boolean projectExists = resourceRepository.projectExists(projectId);

                if (projectExists) {

                    resourceDTO.setId(ResourceMapper.INSTANCE.toDTO(resourceRepository.save(
                            ResourceMapper.INSTANCE.toEntity(resourceDTO))).getId());

                    /** @see ResourceRepository#joinResourceToTheProject(Integer, Integer)  */
                    resourceRepository.joinResourceToTheProject(resourceDTO.getId(), projectId);

                } else {
                    throw new ProjectNotFoundException("Project not found for the ID : " + projectId +
                            " - Impossible to add the resource and link it to the project");
                }
            }
        }
        return resourceDTOList;
    }


                                    /* ===================================== */
                                    /* ============== UPDATE =============== */
                                    /* ===================================== */

    /* -------------------------------------------- UP PROJECT -------------------------------------------- */
    @ApiOperation(value = "UP PROJECT")
    @PutMapping(value = "/project/project/up-project")
    public ProjectDTO upProject (@RequestBody ProjectDTO projectDTO) {

        /**
         * JE VERIFIE SI LE PROJET EXISTE
         */
        Boolean projectExists = projectRepository.existsById(projectDTO.getId());

        if(projectExists) {

            try {
                projectDTO = ProjectMapper.INSTANCE.toDTO(projectRepository.save(ProjectMapper.INSTANCE.toEntity(projectDTO)));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }
        } else {
            logger.error("Update failure - project : '" + projectDTO.getName() + "' - Is not found");
            throw new ProjectNotFoundException("Update failure - project : '" + projectDTO.getName() + "' - Is not found");
        }
        return projectDTO;
    }



                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------------ DEL PROJECT ------------------------------------------ */
    @ApiOperation( value = "DEL PROJECT COMPLETELY BY ID")
    @DeleteMapping( value = "/project/del-project/{projectId}")
    public String delProject(@PathVariable Integer projectId) {

        String massageStatus = "";

        try {

            Integer check = projectRepository.deleteProjectById(projectId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

    /* ------------------------------------ DEL RESOURCE BY PROJECT ------------------------------------ */
    @ApiOperation(value = "DEL RESOURCE BY PROJECT")
    @DeleteMapping(value = "/project/del-resource/{resourceId}/{projectId}")
    public String delResourceByProject(@PathVariable Integer resourceId, @PathVariable Integer projectId) {

        String massageStatus = "";

        try {

            /** @see ResourceRepository#deleteResourceByProject(Integer, Integer) */
            Integer check = resourceRepository.deleteResourceByProject(resourceId, projectId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
