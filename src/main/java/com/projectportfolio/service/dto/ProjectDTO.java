package com.projectportfolio.service.dto;

import lombok.*;
import java.sql.Timestamp;
import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ProjectDTO {

    private Integer id;

    private String name;

    private Timestamp publication;

    private String summarize;

    private List<ImageDTO> imageDTOList;

    private List<ResourceDTO> resourceDTOList;

}
