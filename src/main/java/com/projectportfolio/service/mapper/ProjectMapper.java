package com.projectportfolio.service.mapper;

import com.projectportfolio.model.ProjectEntity;
import com.projectportfolio.service.dto.ProjectDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ProjectMapper {

    ProjectMapper INSTANCE = Mappers.getMapper( ProjectMapper.class );

    ProjectDTO toDTO ( ProjectEntity projectEntity );

    List<ProjectDTO> toDTOList ( List<ProjectEntity> projectEntityList );

    ProjectEntity toEntity ( ProjectDTO projectDTO );

    List<ProjectEntity> toEntityList ( List<ProjectDTO> projectDTOList );


}
