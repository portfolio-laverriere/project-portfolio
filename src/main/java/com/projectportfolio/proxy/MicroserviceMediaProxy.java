package com.projectportfolio.proxy;

import com.projectportfolio.service.dto.ImageDTO;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Headers("Content-Type: application/json")
@FeignClient(name = "MICROSERVICE-MEDIA", url = "http://localhost:9102/MICROSERVICE-MEDIA")
public interface MicroserviceMediaProxy {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DU PROJET
     * @param projectId
     * @return
     */
    @GetMapping(value = "/media/project/get-all-image/{projectId}" )
    List<ImageDTO> getListImageByProject(@PathVariable("projectId") Integer projectId);

}
