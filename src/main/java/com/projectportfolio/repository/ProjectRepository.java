package com.projectportfolio.repository;

import com.projectportfolio.model.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectEntity, Integer> {


    /**
     * SUPPRIMER UN PROJET PAR SON ID
     * @param projectId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM project" +
            " WHERE project.id= :projectId", nativeQuery = true)
    int deleteProjectById(@Param("projectId") Integer projectId);


    /**
     * OBTENIR LE PROJET PAR SON NOM
     * @param projetName
     * @return
     */
    ProjectEntity findByName(String projetName);

}
